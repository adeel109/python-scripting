""" Write a program to find all vowels and their count in a Sentence. """
def vowel_freq(input_str):
    """ Function to count
    vowels in a string """
    freq_vowels = {'a':0, 'e':0, 'i':0, 'o':0, 'u':0}
    final_dict = {}
    for i in input_str:
        if i in freq_vowels:
            freq_vowels[i] += 1
    for key, value in freq_vowels.items():
        if value > 0:
            final_dict[key] = freq_vowels[key]
    return final_dict

if __name__ == '__main__':
    INPUT = input("Enter a string (all lower case) to count vowels: ")
    OUTPUT = vowel_freq(INPUT)
    print("Count of all vowels: {0}".format(OUTPUT))
