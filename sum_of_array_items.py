"""Level 1 Assignment 1"""
import sys

def array_nums(input_array):
    """Function to calculate
    sum of array numbers"""
    add_arr = 0
    for i in input_array:
        add_arr = add_arr + i
    return add_arr

if __name__ == "__main__":
    ARR_INPUT = input("Enter numbers seperated by space for sum: ").split(' ')
    try:
        ARR = [float(num) for num in ARR_INPUT]
        print("Your array is: {0}".format(ARR))
    except ValueError:
        print("Only mumbers are accepted.")
        sys.exit("Please enter numbers only")
    SUM = array_nums(ARR)
    print("Sum is : {0}".format(SUM))
