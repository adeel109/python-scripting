import sys
import platform

print ("My current Python version: ", sys.version) #returns python version and compiler name

print ("\nMy current OS: ", platform.system()) #returns OS name

print ("OS Release: ", platform.release()) #returns OS release information

print ("OS version: ", platform.version()) #returns OS version
