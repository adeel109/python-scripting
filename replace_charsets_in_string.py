""" Level 2 Assignment 1 """
def string_swap(input_str):
    """ This function replaces
    'am' and 'em' by 'my' """
    count_am = 0
    count_em = 0
#    count_fam = 0
    for i in range(0, len(input_str)-1): # loop from 0 to length-1
        test_slice1 = input_str[i:i+2] # picks every two adjascent characters in the string
#        test_slice2 = input_str[i:i+3] # for replacing fam (redundant here)
        if test_slice1 == 'am':
            count_am += 1
        if test_slice1 == 'em':
            count_em += 1
#        if test_slice2 == 'fam': # for replacing fam
#            count_fam += 1
    str_am = input_str.replace('am', 'my', count_am)
    str_em = str_am.replace('em', 'my', count_em)
#    str_fam = str_em.replace('fam', 'fmy', count_fam) # replacing fam
    return str_em  # return final string

if __name__ == "__main__":
    INPUT = input("Enter a string with lots of am's, em's, and fam's ;) : ")
    OUTPUT = string_swap(INPUT)
    print(OUTPUT)
