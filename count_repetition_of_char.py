""" Level 2 Assignment 2 """
def check_freq(input_str):
    """ This functions counts all repeated
    occurances of a letter in a string """
    all_freq = {}
    final_freq = {}
    for i in input_str:
        if i in all_freq:
            all_freq[i] += 1
            final_freq[i] = all_freq[i]
        else:
            all_freq[i] = 1
    return final_freq

if __name__ == "__main__":
    INPUT = input("Enter a string: ")
    OUTPUT = check_freq(INPUT)
    print("{0}".format(OUTPUT))
