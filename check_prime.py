"""Level 1 Assignment 2"""
import sys
def find_prime(check_num):
    """ Function to check if
    number is prime or not """
    for j in range(2, check_num):
        if check_num % j == 0:
            return False
    return True

if __name__ == "__main__":
    print("This code prints prime numbers between 1 to n")
    RANGE = int(input("Enter a number(n) to set limit: "))
    if RANGE <= 0:
        sys.exit("1 is the minimum number accepted")
    if RANGE == 1:
        sys.exit("No prime number found")
    TICK = 0
    PRIME = []
    for i in range(2, RANGE+1):
        status_prime = find_prime(i)
        if status_prime:
            PRIME.append(i)
            TICK = 1
        else:
            continue
    if TICK == 1:
        print("Prime number(s): {0}".format(PRIME))
    else:
        print("No prime number found")
