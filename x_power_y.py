""" Write a program to calculate the power of a number without using
any function. Ex 12 power 2 is 144, 9 power 3 is 729 etc """
def power(x_num, y_num):
    """ This fuction
    calculates exponents"""
    x_power_y = 1
    while y_num != 0:
        x_power_y *= x_num
        y_num -= 1
    return x_power_y
if __name__ == "__main__":
    NUMBER = int(input("Enter number: "))
    EXPONENT = int(input("Enter exponent: "))
    ANSWER = power(NUMBER, EXPONENT)
    print("{0} raised to the power {1} is {2}".format(NUMBER, EXPONENT, ANSWER))
